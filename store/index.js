import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        hasLogin: false,
        userInfo: {}
    },
    mutations: {
        login(state, provider) {
            state.hasLogin = true;
            state.userInfo = provider;
			// uni.setStorage({//将用户信息保存在本地  
			// 				 key: 'userInfo',  
			// 				 data: provider  
			// 			 }) 
        },
        logout(state) {
            state.hasLogin = false;
            state.userInfo = {};
		   // uni.removeStorage({  
					// 	key: 'userInfo'  
					// }) 
        }
    },
    actions: {}
})

export default store
