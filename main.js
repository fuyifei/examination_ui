import Vue from 'vue'
import App from './App'
import store from './store/index.js'
import debounce from './util/debounce.js'

Vue.config.productionTip = false
Vue.prototype.$store = store;
Vue.prototype.$eventHub = new Vue();
Vue.prototype.debounce = debounce.debounce;
App.mpType = 'app'

const app = new Vue({
	store,
	debounce,
    ...App
})
app.$mount()
