/**
 * 函数防抖使用方法
 *      在页面直接调用
 *      @click="debounce(functionName, args,wait)"
 *  注意：暂时只是适用只有一个参数的情况（一个参数位置，参数可以是一个数组也可以是对象等）
 *      全局变量 timeout，定时器设置
 *      全局变量funcName，防抖的方法
 *      
 *  在wait的时间内连续触发函数时会将执行函数的时间不断向后推迟；
 *  更换请求方法之后就主动清除定时器；
 *  clearTimeout()该方法只是停止定时器，并未对定时器产生的id有何影响
 *  
 */
let timeout
let funcName
const debounce = {
    /**
     * 
     * @param {必填} func  传递过来要执行的函数方法
     * @param {选填} args  需要向函数传递的参数，在func方法需要页面传递参数时候传值，否则就是null
     * @param {选填} wait  时间间隔，在wait时间内只能执行一次传递过来的方法，默认是2000毫秒，做了缺省参数
     */
    // debounce: function (func, args, wait = 2000) {
    //     if (funcName != func) {
    //         timeout = null;
    //         funcName = func
    //     }
    //     let context = this;

    //     //每一次进来都是停止上一次定时器
    //     if (timeout) clearTimeout(timeout);

    //     let callNow = !timeout;
    //     timeout = setTimeout(() => {
    //         timeout = null;
    //     }, wait)
    //     if (callNow) func.call(context, args)
    // },

    /**
     * 第一种写法是会先执行一次，然后再点击之后会执行定时，直到定时结束，再次点击才会执行；
     * 第二种写法是一直不会执行，直到执行了定时；
     */
    debounce: function (func, args, wait = 1000) {
        if (funcName != func) {
            timeout = null;
            funcName = func
        }
        let context = this;

        //每一次进来都是停止上一次定时器
        if (timeout) clearTimeout(timeout);

        timeout = setTimeout(() => {
            func.call(context, args)
        }, wait)
    }
}

export default debounce